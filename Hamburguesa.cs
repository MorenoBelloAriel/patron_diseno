﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patron_Decorador
{
    public class Hamburguesa : Comida_Rapida//Usando Herencia a clases Hijas/Derivadas de la clase Base abstracta (Comida_Rapida)
    {
        //herendando las propiedades de la clase base abstracta ( comida_rapida)
        public override double Precio => 8;

        public override string Descripcion => "Pan de hamburguesa con carne y ensalada";
    }
}
