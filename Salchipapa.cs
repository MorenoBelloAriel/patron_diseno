﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patron_Decorador
{
    public class Salchipapa : Comida_Rapida//Usando Herencia a clases Hijas/Derivadas de la clase Base abstracta (Comida_Rapida)
    {
        //herendando las propiedades de la clase base abstracta ( comida_rapida) y asignando sus respectivos Datos
    
        public override double Precio => 5;
        public override string Descripcion => "Papas con chorizo ";
    }
}
