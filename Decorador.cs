﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patron_Decorador
{
    public abstract class Decorador:Comida_Rapida//clase siendo abstracta que tiene heredacion de comida_rapida. y tiene una agregacion con Comida rapida.
    {
        protected Comida_Rapida _comidas;//propiedad que sera usada para las clases derivada o clases hijos 
        public Decorador(Comida_Rapida comida )//instancia de comida 
        {
            _comidas = comida;
        }
        
    }
}
