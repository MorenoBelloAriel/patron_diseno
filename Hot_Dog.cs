﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patron_Decorador
{
    public class Hot_Dog: Comida_Rapida//Usando Herencia a clases Hijas/Derivadas de la clase Base abstracta (Comida_Rapida)
    {
        //herendando las propiedades de la clase base abstracta ( comida_rapida) y asignando valores a los datos que requiere.
    
        public override double Precio => 8;

        public override string Descripcion => "Pan con chorizo/embutido ";
    }
}
