﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patron_Decorador
{
    public abstract class Comida_Rapida
    {
        //CLASE BASE HACIENDO QUE SEA UNA CLASE ABSTRACTA.
        //Sus propiedades  conviertiendolas en abstracta para ser usadas como el polimorfosis, a sus clases derivadas.
        public abstract double Precio { get; }
        public abstract string Descripcion { get; }
    }
}
