﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patron_Decorador
{
    public class Picante:Decorador
    {
        public Picante(Comida_Rapida _comidas) : base(_comidas) { }//Definiendo el producto a la cual se le agregara la clase concreta 
        //agregando el costo de ya viene agregado por la AGREGACION de _comidas.
        public override double Precio => _comidas.Precio + 3;

        public override string Descripcion => string.Format($"{_comidas.Descripcion}, Salsa Picante");
    }
}
