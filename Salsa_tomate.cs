﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patron_Decorador
{
    public class Salsa_tomate : Decorador
    {
        public Salsa_tomate(Comida_Rapida _comidas) : base(_comidas) { }//Definiendo el producto a la cual se le agregara la clase concreta 
        //agregando el costo de ya viene agregado por la AGREGACION de _comidas.
        public override double Precio => _comidas.Precio + 1.01;

        public override string Descripcion => string.Format($"{_comidas.Descripcion},Salsa de tomate");
    }
}
