﻿using System;

namespace Patron_Decorador
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("================Bienvenido a el carrito de comida rapida==================");
            Comida_Rapida Comida = new Salchipapa();//instancia del objeto "comida" apartir de la clase salchipapa 
            //agregacion del objeto comida  una nueva funcionalidad mediante el patron decorador que en este caso es la clase concreta.
            Comida = new Picante(Comida);///agregando picante a la comida " que en este caso es salchipapa 
            Comida = new Salsa_tomate(Comida);//agregando salsa de tomate a la comida " que en este caso es salchipapa
            Comida = new Mayonesa(Comida);//agregando Mayonesa a la comida " que en este caso es salchipapa
            Comida = new Mostaza(Comida);//agregando Mostaza a la comida " que en este caso es salchipapa
            Comida = new Mayonesa(Comida);//agregando Mayonesa a la comida " que en este caso es salchipapa

            //mostrando por mensaje la descripcion de cada uno de las clases concretas y el precio de cada uno ya sumado es decir su total.
            Console.WriteLine($"Descripcion de los productos  :   \n-->{ Comida.Descripcion }\nPrecio total :  \n-->${Comida.Precio}");
            Console.ReadKey();

        }
    }
}
